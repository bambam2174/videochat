const request = require('supertest');
const app = require('../app');

describe('App', function () {
  it('has the default page', function (done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });

  it('has a users page', done => {
    request(app)
      .get('/users')
      .expect(/respond with a resource/, done);
  });

  it('users page answers HTTP 200', done => {
    request(app)
      .get('/users')
      .expect(200, done);
  });


}); 
