const request = require('supertest');
const socketApp = require('../sockets');

describe('SocketApp', function () {
    it('has the default page', function (done) {
        request(socketApp)
            .get('/')
            .expect(/Welcome to Express/, done);
    });

    it('has a users page', done => {
        request(socketApp)
            .get('/users')
            .expect(/respond with a resource/, done);
    });

    it('users page answers HTTP 200', done => {
        request(socketApp)
            .get('/users')
            .expect(200, done);
    });


    it('chats page check link text', done => {
        request(socketApp)
            .get('/chats')
            .expect(/Another Chat Window/, done);
    });



    it('chats page query video', done => {
        request(socketApp)
            .get('/chats').then((...args) => {
                console.log(args);
                const parser = new DOMParser();
                const doc = parser.parseFromString(args[0].text, "text/html");
                console.log(doc);
                return args;
            })
            .expect(/Another Chat Window/, done);
    });

}); 
