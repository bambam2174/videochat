'use strict';

// imports
const os = require('os');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const http = require('http');
// signalling for WebRTC
const socketIO = require('socket.io');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const chatsRouter = require('./routes/chat');
// initialize express object
const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'dist')));

// define dist-folder for CSS-files and other static files
// app.use(express.static('dist'));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/chats', chatsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// create http server instance using express instance
const serverSocketIO = http.createServer(app);

// initializing socketIO with http-server
const io = socketIO(serverSocketIO);

// implementing socketIO listeners

io.on('connection', socket => {

    function log() {
        let arrMessage = ['SERVER-Message'];
        arrMessage.push.apply(arrMessage, arguments);
        socket.emit('log', arrMessage);
    }

    function numberOFClientsInRoom(room, nsp = '/') {
        let allRooms = io.sockets.adapter.rooms;
        log2client(socket, 'io.sockets.adapter.rooms', allRooms);

        socket.emit('rooms-state-changed', allRooms, room);

        let clients = allRooms.get(room);
        log2client(socket, 'allRooms.get(room)', clients, ', clients.size', clients ? clients.size : 0);

        return clients ? clients.size : 0;
    }

    // defining socket behaviour on events emitted:

    // directing message to associated room
    // on `message event`
    socket.on('message', (message, room) => {
        log2client(socket, 'Client said: ', message);
        socket.to(room).emit('message', message, room);
    });

    socket.on('create or join', room => {
        log2client(socket, 'Received request to create or join room: ', room);

        let numberOfClients = numberOFClientsInRoom(room);

        log2client(socket, `Room ${room} has now ${numberOfClients} client ${(numberOfClients > 1) ? 's' : ''}`);

        if (numberOfClients === 0) {
            socket.join(room);
            log2client(socket, `Client ID ${socket.id} created room ${room}`);
            socket.emit('created', room, room.id);
        } else if (numberOfClients === 1) {
            log(`Client ID ${socket.id} joined the room ${room}`);
            io.sockets.in(room).emit('join', room);
            socket.join(room);
            socket.emit('joined', room, socket.id);
            io.sockets.in(room).emit('ready');
        } else { // 2 clients max
            socket.emit('full', room);
        }

        socket.on('ipaddr', (...args) => {
            log2client(socket, 'socket on ipaddr', args);
            const networkInterfaces = os.networkInterfaces;
            log2client(socket, 'Network Interfaces', networkInterfaces);
            for (const dev in networkInterfaces) {
                networkInterfaces[dev].forEach(details => {
                    log2client(socket, 'Details of interface ' + dev, details, details.family, details.address);
                    if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
                        socket.emit('ipaddr', details.address);
                    }
                });
            }
        });

        socket.on('bye', (...args) => {
            log2client(socket, 'bye', args);
        });
    });

});

function log2client(socket, ...args) {
    console.log('args:', ...args);
    let arrMessage = ['SERVER-Message'];
    arrMessage.push.apply(arrMessage, args);
    socket.emit('log', arrMessage);
}

module.exports = serverSocketIO;