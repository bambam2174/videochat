const express = require('express');
const router = express.Router();
const logger = require('morgan');

router.use(logger('dev'))
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.render('users', {
      title: 'Active Chat Rooms',
      fubar: req.query.fubar || 'nix',
      room: req.query.room || 'none',
      hostname: req.hostname, 
      protocol: req.protocol,
      pathname: 'users'
    });
  console.log('routes/users', 'reg', req, 'req.constructor.name', req.constructor.name, 'res', res);
  console.log('routes/users II', 'req.hostname', req.hostname || 'undefined');
});

module.exports = router;
