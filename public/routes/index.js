const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
  console.log('router Home', 'reg', req, 'res', res);
  console.log('routes/Home II', 'req.hostname', req.hostname || 'undefined');
  res.render('index', { title: 'Express', fubar: req.query.fubar || 'nix', hostname: req.hostname || 'undefined', protocol: req.protocol || 'undefined' });
});

module.exports = router;
