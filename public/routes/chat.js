const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    console.log('router chat', 'reg', req, req.host || '', 'res', res);
    res.render('chat', {
        title: 'VideoChatRoom',
        fubar: req.query.fubar || 'nix',
        room: req.query.room || 'none'
    });
});

module.exports = router;